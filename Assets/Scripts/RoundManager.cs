﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace test
{
	public enum TurnState { PreTurn, Planning, Execution, PostTurn, GameOver }

	public class RoundManager : MonoBehaviour
	{
		public bool Run = false;
		private bool Running = false;

		public string Name;

		public Text RoundMarquee;

		//public List<RoundActor> RoundActors;
		public List<Unit> Units;

		public int CurrentRound;
		public float RoundTime;

		public TurnState CurrentTurnState;

		public Dropdown ActionPicker;
		public Button ActionSelect;
		public Button ReadyButton;

		private bool Waiting = false;

		private ActorManager ActorManager;
		private UnitManager UnitManager;

		// Use this for initialization
		protected virtual void Start()
		{
			CurrentTurnState = TurnState.PreTurn;
			ActorManager = GameObject.FindGameObjectWithTag("ActorManager").GetComponent<ActorManager>();
			UnitManager = GameObject.FindGameObjectWithTag("UnitManager").GetComponent<UnitManager>();
			//RoundActors = new List<RoundActor>(GameObject.FindGameObjectWithTag("ActorManager").GetComponent<ActorManager>().Actors.Values);

			if (RoundMarquee != null)
			{
				RoundMarquee.text = "";
			}
		}

		// Update is called once per frame
		void Update()
		{
			if(Running && !Run)
			{
				Running = false;
				return;
			}
			if(Run && !Running)
			{
				RoundStart();
			}

			if (!Running || Waiting)
				return;

			RoundUpdate();

			foreach(Unit unit in Units)
			{
				unit.Actor.ActorUpdate(unit);
			}

		}

		protected virtual void RoundUpdate()
		{
			UpdateMessage(PreRoundMessage());
			
		}

		protected virtual void RoundStart()
		{
			Units = new List<Unit>(UnitManager.Units.Values);
			foreach (Unit unit in Units)
			{
				unit.Actor.Initialize(this, unit);
			}

			Running = true;
			Waiting = false;
			CurrentRound = 0;
			CurrentTurnState = TurnState.PreTurn;
		}

		protected void UpdateMessage(string message)
		{
			if (message == null)
				return;

			if (RoundMarquee != null)
			{
				RoundMarquee.text = message;
			}
			Debug.Log(message);
		}

		protected IEnumerator WaitTimer(float seconds, TurnState state)
		{
			if (!Waiting)
			{
				Waiting = true;
				yield return new WaitForSeconds(seconds);
				CurrentTurnState = state;
				Waiting = false;
			}

		}

		public void NextRound()
		{
			CurrentTurnState = TurnState.Execution;
			++CurrentRound;
			UpdateMessage(NextRoundMessage());
		}

		public virtual string NextRoundMessage()
		{
			return string.Format("TIME FOR ROUND {0}!", CurrentRound);
		}

		public virtual string PreRoundMessage()
		{
			return string.Format("TIME FOR ROUND {0}!", CurrentRound);
		}
	}

}
