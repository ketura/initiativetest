﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class ActorPanel : MonoBehaviour
	{
		public Text NameBox;
		public Text StatusBox;
		public Text ActionBox;
		void Start()
		{

		}

		public void Populate(string name, string status)
		{
			NameBox.text = name;
			StatusBox.text = status;
		}

		void Update()
		{

		}
	}
}