﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class UnitComparer : Comparer<Unit>
	{
		public override int Compare(Unit x, Unit y)
		{
			return x.Speed - y.Speed;
		}
	}
	public class Unit : MonoBehaviour, IActorUnit
	{
		public string Name;
		public Species SpeciesType;
		public RoundActor Actor;
		public int Speed;
		public List<Action> DefaultMoves;
		public List<Action> Moves;

		public ActorPanel Panel;

		private ActionManager ActionManager;

		public bool Ready { get; set; }

		public bool Finished { get; set; }

		public string Message
		{
			get
			{
				if(Panel != null)
				{
					return Panel.StatusBox.text;
				}
				return "";
			}

			set
			{
				if (Panel != null)
				{
					Panel.StatusBox.text = value;
				}
			}
		}

		public string MoveMessage
		{
			get
			{
				if (Panel != null)
				{
					return Panel.ActionBox.text;
				}
				return "";
			}

			set
			{
				if (Panel != null)
				{
					Panel.ActionBox.text = value;
				}
			}
		}

		public List<Action> PlannedActions { get; set; }

		void Start()
		{
			GameObject.FindGameObjectWithTag("UnitManager").GetComponent<UnitManager>().AddUnit(this);
			ActionManager = GameObject.FindGameObjectWithTag("ActionManager").GetComponent<ActionManager>();

			Moves = new List<Action>();
			if (DefaultMoves == null)
				DefaultMoves = new List<Action>();

			if (Actor != null)
			{
				Moves.AddRange(Actor.DefaultMoves);
			}

			if (SpeciesType != null)
			{
				foreach (Action a in SpeciesType.DefaultMoves)
				{
					Moves.Add(a);
				}
			}

			Moves.AddRange(DefaultMoves);

			//lazy hack to remove dupes
			Moves = new List<Action>(new HashSet<Action>(Moves));
		}

		void Update () 
		{
		
		}
	}
}
