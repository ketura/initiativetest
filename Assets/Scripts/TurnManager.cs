﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class TurnManager : MonoBehaviour 
	{
		[SerializeField]
		public Dictionary<string, TurnType> TurnTypes;
		void Awake()
		{
			if (TurnTypes == null)
			{
				TurnTypes = new Dictionary<string, TurnType>();
			}
		}

		public void AddTurnType(TurnType turntype)
		{
			if (TurnTypes == null)
			{
				TurnTypes = new Dictionary<string, TurnType>();
			}

			if (TurnTypes.ContainsKey(turntype.Name))
			{
				Debug.Log(string.Format("Warning!  Turn Type {0} has been submitted again to the Turn Registery.", turntype.Name));
			}

			TurnTypes[turntype.Name] = turntype;
		}

		public TurnType GetTurnType(string name)
		{
			if (!TurnTypes.ContainsKey(name))
				return null;

			return TurnTypes[name];
		}

		public IEnumerable<string> ActorNames
		{
			get
			{
				return TurnTypes.Keys;
			}

		}
		void Update()
		{

		}
	}
}
