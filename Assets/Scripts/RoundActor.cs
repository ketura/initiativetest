﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using Utilities

namespace test
{
	[RequireComponent(typeof(MoveList))]
	public class RoundActor : MonoBehaviour
	{
		public string Name;

		public List<Action> DefaultMoves;

		public List<Action> AllMoves;

		[SerializeField]
		protected ActionManager ActionManager;
		protected RoundManager RoundManager;
		protected UnitManager UnitManager;

		public virtual bool IsReady(Unit unit)
		{
			return unit.Ready;
		}

		public virtual bool IsFinished(Unit unit)
		{
			return unit.Finished;
		}

		public void SetMoves(Unit unit)
		{
			AllMoves = new List<Action>();
			AllMoves.AddRange(DefaultMoves);
			AllMoves.AddRange(unit.SpeciesType.DefaultMoves);
		}

		protected virtual void Start()
		{
			//GameObject.FindGameObjectWithTag("ActorManager").GetComponent<ActorManager>().AddActor(this);
			ActionManager = GameObject.FindGameObjectWithTag("ActionManager").GetComponent<ActionManager>();
			GameObject.FindGameObjectWithTag("ActorManager").GetComponent<ActorManager>().AddActor(this);
			UnitManager = GameObject.FindGameObjectWithTag("UnitManager").GetComponent<UnitManager>();
		}

		public virtual void Initialize(RoundManager rm, Unit unit)
		{
			RoundManager = rm;
			unit.PlannedActions = new List<Action>();
			unit.Message = "Not Ready";
		}

		public virtual void ActorUpdate(Unit unit)
		{
			unit.Message = IsReady(unit) ? "Ready" : "Not Ready";
		}

		public virtual void StartPlanning(Unit unit)
		{
			unit.Ready = false;
			unit.Message = "Not Ready";
			unit.PlannedActions.Clear();
		}

		public virtual IEnumerable<Action> StartExecution(Unit unit)
		{
			unit.Finished = false;
			List<Action> copy = new List<Action>(unit.PlannedActions);
			
			unit.MoveMessage = MoveMessage(unit);

			unit.PlannedActions.Clear();
			return copy;
		}

		public virtual string MoveMessage(Unit unit)
		{
			string moves = "";
			//Debug.Log(string.Format("Actor {0} has a {1} list that has {2} items.", this.Name, copy == null ? "null" : "not null", copy.Count));
			foreach (Action action in unit.PlannedActions)
			{
				moves += action.Name + ", ";
			}
			//Debug.Log(moves);
			if(moves.Length > 0)
				moves = moves.Remove(moves.Length - 2);

			return moves;
		}
	}

	public interface IActorUnit
	{
		bool Ready { get; set; }
		bool Finished { get; set; }

		string Message { get; set; }
		string MoveMessage { get; set; }
		List<Action> PlannedActions { get; set; }
	}
}
