﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class MoveAdder : MonoBehaviour 
	{
		public Dropdown MovePicker;
		public Button AddButton;
		public Button RemoveButton;
		public RectTransform MovePanel;

		public Dictionary<string, Text> Moves;

		private DropdownClearDefault PickerDefault;
		private ActionManager ActionManager;
		void Start () 
		{
			PickerDefault = MovePicker.GetComponent<DropdownClearDefault>();
			ActionManager = GameObject.FindGameObjectWithTag("ActionManager").GetComponent<ActionManager>();
			foreach (var move in ActionManager.ActionNames)
			{
				MovePicker.options.Add(new Dropdown.OptionData(move));
			}
		}

		public void AddClicked()
		{
			if (MovePicker.itemText.text == PickerDefault.DefaultName)
				return;
		}

		public void RemoveClicked()
		{
			if (MovePicker.itemText.text == PickerDefault.DefaultName)
				return;


		}


		void Update () 
		{
		
		}
	}
}
