﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class Action : MonoBehaviour 
	{
		public string Name;
		public bool Enabled;
		void Start () 
		{
			GameObject.FindGameObjectWithTag("ActionManager").GetComponent<ActionManager>().AddAction(this);
			Enabled = true;
		}

		void Update () 
		{
		
		}
	}
}
