﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class ActionManager : MonoBehaviour 
	{
		private Dictionary<string, Action> Actions;

		public IEnumerable<string> ActionNames
		{
			get
			{
				return Actions.Keys;
			}
		}

		public Action GetAction(string name)
		{
			if (!Actions.ContainsKey(name))
				return null;

			return Actions[name];
		}

		void Awake () 
		{
			Actions = new Dictionary<string, Action>();
		}

		public void AddAction(Action action)
		{
			if(Actions.ContainsKey(action.Name))
			{
				Debug.Log(string.Format("Warning!  Action {0} has been submitted again to the Action Registery.", action.Name));
			}

			Actions[action.Name] = action;
		}

		void Update () 
		{
		
		}
	}
}
