﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class Species : MonoBehaviour 
	{
		public string Name;
		public int Speed;
		public List<Action> DefaultMoves;

		void Awake () 
		{
			GameObject.FindGameObjectWithTag("SpeciesManager").GetComponent<SpeciesManager>().AddSpecies(this);
		}

		void Update () 
		{
		
		}
	}
}
