﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class SimultaneousRoundManager : RoundManager
	{
		protected override void RoundUpdate()
		{
			switch (CurrentTurnState)
			{
				case TurnState.PreTurn:
					StartCoroutine(WaitTimer(RoundTime / 2, TurnState.Planning));
					foreach (var unit in Units)
					{
						unit.Actor.StartPlanning(unit);
					}
					break;
				case TurnState.Planning:
					bool ready = true;
					foreach (var unit in Units)
					{
						if (!unit.Actor.IsReady(unit))
						{
							ready = false;
							//Debug.Log("Waiting on " + actor.Name);
						}
					}

					if (ready)
					{
						NextRound();
					}
					break;
				case TurnState.Execution:
					var Actions = new Dictionary<RoundActor, IEnumerable<Action>>();

					foreach (var unit in Units)
					{
						Actions[unit.Actor] = unit.Actor.StartExecution(unit);
					}


					CurrentTurnState = TurnState.PostTurn;

					break;
				case TurnState.PostTurn:
					StartCoroutine(WaitTimer(RoundTime / 2, TurnState.PreTurn));
					break;
				case TurnState.GameOver:
					break;
				default:
					break;
			}
		}
	}
}
