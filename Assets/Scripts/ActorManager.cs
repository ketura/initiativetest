﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class ActorManager : MonoBehaviour 
	{
		public GameObject ActorPanelPrefab;
		public RectTransform MasterPanel;
		[SerializeField]
		public Dictionary<string, RoundActor> Actors;
		void Awake()
		{
			Actors = new Dictionary<string, RoundActor>();
		}

		public void AddActor(RoundActor actor)
		{
			Actors[actor.Name] = actor;
			//if (actor.Panel == null)
			//{
			//	GameObject go = GameObject.Instantiate(ActorPanelPrefab, MasterPanel, false);
			//	actor.Panel = go.GetComponent<ActorPanel>();
			//}
		}

		public RoundActor GetActor(string name)
		{
			if (!Actors.ContainsKey(name))
				return null;

			return Actors[name];
		}

		public IEnumerable<string> ActorNames
		{
			get
			{
				return Actors.Keys;
			}

		}
		void Update () 
		{
		
		}
	}
}
