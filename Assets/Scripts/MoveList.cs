﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class MoveList : MonoBehaviour 
	{
		[SerializeField]
		public List<string> MovesToLoad;
		public List<Action> Moves;
		void Start () 
		{
			Moves = new List<Action>();
			var ActionManager = GameObject.FindGameObjectWithTag("ActionManager").GetComponent<ActionManager>();
			foreach(string s in MovesToLoad)
			{
				Action a = ActionManager.GetAction(s);
				if (a != null)
					Moves.Add(a);
			}
		}

		void Update () 
		{
		
		}
	}
}
