﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
//using Utilities

namespace test
{
	public class SimulatorScene : MonoBehaviour 
	{
		public GameObject ActorPanelPrefab;
		public RectTransform MasterPanel;

		public Dropdown ActionPicker;
		public Button ActionSelect;
		public Button ReadyButton;
		public Text RoundMarquee;

		public Button ReturnButton;

		private SourceInfo SourceInfo;
		private ActionManager ActionManager;
		private SpeciesManager SpeciesManager;
		private UnitManager UnitManager;
		private ActorManager ActorManager;

		private RoundManager RoundManager;
		void Start () 
		{
			ActionManager = GameObject.FindGameObjectWithTag("ActionManager").GetComponent<ActionManager>();
			SpeciesManager = GameObject.FindGameObjectWithTag("SpeciesManager").GetComponent<SpeciesManager>();
			UnitManager = GameObject.FindGameObjectWithTag("UnitManager").GetComponent<UnitManager>();
			ActorManager = GameObject.FindGameObjectWithTag("ActorManager").GetComponent<ActorManager>();

			SourceInfo = GameObject.FindGameObjectWithTag("SourceInfo").GetComponent<SourceInfo>();

			ActorManager.MasterPanel = MasterPanel;

			RoundManager = GameObject.FindGameObjectWithTag(SourceInfo.SelectedTurnType).GetComponent<TurnType>().TurnHandler;

			RoundManager.ActionPicker = ActionPicker;
			RoundManager.ActionSelect = ActionSelect;
			RoundManager.ReadyButton = ReadyButton;
			RoundManager.RoundMarquee = RoundMarquee;

			foreach (var unit in UnitManager.Units.Values)
			{

				ActorPanel panel = Instantiate(ActorPanelPrefab, MasterPanel, false).GetComponent<ActorPanel>();
				panel.Populate(unit.Name, "Ready");
				unit.Panel = panel;

			}

			RoundManager.Run = true;

			ReturnButton.onClick.AddListener(ReturnClicked);
		}

		public void ReturnClicked()
		{
			SourceInfo.OriginScene = "Simulator";

			SceneManager.LoadScene("ActorSelector");
			
		}
	}
}
