﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class TurnSelectorScene : MonoBehaviour
	{
		public Button ContinueButton;
		public ToggleGroup MasterPanel;
		public GameObject TurnRadioPrefab;
		public SourceInfo SourceInfo;

		private TurnManager TurnManager;

		private Dictionary<string, Toggle> Toggles;
		void Start()
		{
			ContinueButton.onClick.AddListener(ContinueClicked);
			SourceInfo = GameObject.FindGameObjectWithTag("SourceInfo").GetComponent<SourceInfo>();

			TurnManager = GameObject.FindGameObjectWithTag("TurnManager").GetComponent<TurnManager>();

			Toggles = new Dictionary<string, Toggle>();
			foreach(var turntype in TurnManager.TurnTypes.Values)
			{
				GameObject go = Instantiate(TurnRadioPrefab, MasterPanel.gameObject.transform, false);
				Toggle t = go.GetComponent<Toggle>();
				t.group = MasterPanel;
				t.GetComponentInChildren<Text>().text = string.Format("{0}: {1}", turntype.Name, turntype.Description);

				Toggles[turntype.Name] = t;
			}

			//dumb hack to ensure the order in the gui is proper
			foreach (var turntype in TurnManager.TurnTypes.Values)
			{
				Toggles[turntype.Name].transform.SetSiblingIndex(0);
			}

			if (SourceInfo.SelectedTurnType != "")
			{
				Toggles[SourceInfo.SelectedTurnType].isOn = true;
			}
			else
			{
				MasterPanel.transform.GetChild(0).GetComponent<Toggle>().isOn = true;
			}

			

		}

		public void ContinueClicked()
		{
			//this foreach is only here because friggin unity won't let me use ActiveToggles.First()
			foreach (Toggle t in MasterPanel.ActiveToggles())
			{
				foreach(var pair in Toggles)
				{
					if(pair.Value == t)
					{
						SourceInfo.SelectedTurnType = pair.Key;
					}
				}
			}

			if (SourceInfo.OriginScene != "")
			{
				SceneManager.LoadScene(SourceInfo.OriginScene);
			}
			else
			{
				SceneManager.LoadScene("Simulator");
			}
		}

		void Update()
		{

		}
	}
}
