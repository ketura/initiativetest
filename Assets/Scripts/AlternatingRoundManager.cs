﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class AlternatingRoundManager : RoundManager
	{
		public Unit[] OrderedUnits;
		public int CurrentIndex;

		protected override void Start()
		{
			base.Start();
		}

		protected override void RoundStart()
		{
			base.RoundStart();

			OrderedUnits = new Unit[Units.Count];
			Units.Sort((x, y) => { return y.Speed - x.Speed; });
			OrderedUnits = Units.ToArray();
			CurrentIndex = 0;
		}

		protected override void RoundUpdate()
		{
			Unit unit = OrderedUnits[CurrentIndex];

			switch (CurrentTurnState)
			{
				case TurnState.PreTurn:
					StartCoroutine(WaitTimer(RoundTime / 2, TurnState.Planning));

					unit.Actor.StartPlanning(unit);

					break;
				case TurnState.Planning:
					if (unit.Actor.IsReady(unit))
					{
						NextRound();
					}

					break;
				case TurnState.Execution:
					var Actions = new Dictionary<RoundActor, IEnumerable<Action>>();

					Actions[unit.Actor] = unit.Actor.StartExecution(unit);

					CurrentTurnState = TurnState.PostTurn;

					break;
				case TurnState.PostTurn:
					CurrentIndex++;
					if (CurrentIndex >= OrderedUnits.Length)
						CurrentIndex = 0;

					StartCoroutine(WaitTimer(RoundTime / 2, TurnState.PreTurn));
					break;
				case TurnState.GameOver:
					break;
				default:
					break;
			}

			base.RoundUpdate();
		}

		public override string PreRoundMessage()
		{
			return string.Format("{0}'s Turn!", Units[CurrentIndex].Name);
		}

		public override string NextRoundMessage()
		{
			return null;
		}

	}
}
