﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class ActorSelectorScene : MonoBehaviour 
	{
		public Button ContinueButton;
		public SourceInfo SourceInfo;
		public GameObject Managers;
		void Start () 
		{
			ContinueButton.onClick.AddListener(ContinueClicked);
			SourceInfo = GameObject.FindGameObjectWithTag("SourceInfo").GetComponent<SourceInfo>();
			DontDestroyOnLoad(Managers);
		}

		public void ContinueClicked()
		{
			SourceInfo.Units = new List<Unit>();
			foreach(Unit unit in GameObject.FindGameObjectWithTag("UnitManager").GetComponent<UnitManager>().Units.Values)
			{
				SourceInfo.Units.Add(unit);
				//DontDestroyOnLoad(unit.gameObject);
				//unit.transform.parent = SourceInfo.transform;
			}

			if (SourceInfo.OriginScene != "")
			{
				SceneManager.LoadScene(SourceInfo.OriginScene);
			}
			else
			{
				SceneManager.LoadScene("TurnSelector");
			}
		}

		void Update () 
		{
		
		}
	}
}
