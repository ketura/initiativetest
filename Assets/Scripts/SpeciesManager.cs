﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class SpeciesManager : MonoBehaviour 
	{
		public Dictionary<string, Species> Species;

		public IEnumerable<string> SpeciesNames
		{
			get
			{
				return Species.Keys;
			}
		}

		public Species GetSpecies(string name)
		{
			if (!Species.ContainsKey(name))
				return null;

			return Species[name];
		}

		public void AddSpecies(Species species)
		{
			if(Species == null)
			{
				Species = new Dictionary<string, test.Species>();
			}

			if (Species.ContainsKey((string)species.Name))
			{
				Debug.Log(string.Format("Warning!  Species {0} has been submitted again to the Species Registery.", (object)species.Name));
			}

			Species[species.Name] = species;
		}

		void Awake () 
		{
			
		}

		void Update () 
		{
		
		}
	}
}
