﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
//using Utilities

namespace test
{
	public class UnitCreator : MonoBehaviour 
	{
		public UnitPanel UnitPanel;
		public RectTransform MasterPanel;
		public GameObject UnitPanelPrefab;
		public Text MessageBox;

		private DropdownClearDefault SpeciesDDClear;
		private DropdownClearDefault ActorDDClear;

		private ActionManager ActionManager;
		private SpeciesManager SpeciesManager;
		private UnitManager UnitManager;
		private ActorManager ActorManager;

		void Start () 
		{
			ActionManager = GameObject.FindGameObjectWithTag("ActionManager").GetComponent<ActionManager>();
			SpeciesManager = GameObject.FindGameObjectWithTag("SpeciesManager").GetComponent<SpeciesManager>();
			UnitManager = GameObject.FindGameObjectWithTag("UnitManager").GetComponent<UnitManager>();
			ActorManager = GameObject.FindGameObjectWithTag("ActorManager").GetComponent<ActorManager>();

			SpeciesDDClear = UnitPanel.SpeciesPicker.GetComponent<DropdownClearDefault>();
			ActorDDClear = UnitPanel.ActorPicker.GetComponent<DropdownClearDefault>();

			SpeciesDDClear.AddRange(SpeciesManager.Species.Keys);
			ActorDDClear.AddRange(ActorManager.Actors.Keys);

			UnitPanel.UnitButton.onClick.AddListener(AddUnitClicked);

			MessageBox.text = "";
		}

		public void AddUnitClicked()
		{
			string message = "";
			
			//Debug.Log(SpeciesPicker.options.ToArray()[SpeciesPicker.value].text);
			if (UnitPanel.SpeciesPicker.itemText.text == SpeciesDDClear.DefaultName)
			{
				message = "Must select a Species!";
			}
			else if (UnitPanel.ActorPicker.itemText.text == ActorDDClear.DefaultName)
			{
				message = "Must select an Actor!";
			}
			

			MessageBox.text = message;
			if (message != "")
				return;

			var names = new List<string>(UnitManager.UnitNames);

			if (UnitPanel.UnitNameText.text == "")
			{
				UnitPanel.UnitNameText.text = UnitPanel.SpeciesPicker.itemText.text;
			}

			string name = UnitPanel.UnitNameText.text;

			if (names.Contains(name))
			{
				Match match = Regex.Match(name, @"(.*?)(\d*)$");
				string corename = match.Groups[1].Value;
				int num = 1;
				string temp = match.Groups[2].Value;
				if (!string.IsNullOrEmpty(temp))
					num = int.Parse(temp);
				//need to do a regex search for digits at the end
				while (names.Contains(name))
				{
					name = corename + num;
					num++;
				}
			}

			UnitPanel.UnitNameText.text = name;

			bool create = UnitManager.GetUnit(name) == null;

			var unit = UnitManager.CreateUnit(name,
									UnitPanel.SpeciesPicker.itemText.text,
									UnitPanel.ActorPicker.itemText.text,
									UnitPanel.UnitSpeedText.text == "" ? 0 : int.Parse(UnitPanel.UnitSpeedText.text)
			);

			if (create)
			{
				UnitPanel panel = Instantiate(UnitPanelPrefab, MasterPanel, false).GetComponent<UnitPanel>();
				panel.Populate(unit, ActorManager.Actors.Keys, SpeciesManager.Species.Keys);
				panel.UnitButton.GetComponentInChildren<Text>().text = "Remove Unit";
				panel.ClickCallback = (Unit u) =>
				{
					foreach (var child in MasterPanel.transform.GetComponentsInChildren<UnitPanel>())
					{
						if (child.Unit.Name == u.Name)
						{
							Destroy(child.gameObject);
							break;
						}
					}

					UnitManager.DestroyUnit(u);
				};
			}
		}

		void Update () 
		{
		
		}
	}
}
