﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class UnitPanel : MonoBehaviour 
	{
		public Dropdown SpeciesPicker;
		public Dropdown ActorPicker;
		public InputField UnitNameText;
		public InputField UnitSpeedText;
		public Button UnitButton;

		public Unit Unit;
		public System.Action<Unit> ClickCallback;

		public void Populate(Unit unit, IEnumerable<string> actors, IEnumerable<string> species)
		{
			SpeciesPicker.GetComponent<DropdownClearDefault>().AddRange(species);
			ActorPicker.GetComponent<DropdownClearDefault>().AddRange(actors);

			SpeciesPicker.value = SpeciesPicker.options.FindIndex(x => x.text == unit.SpeciesType.Name);
			ActorPicker.value = ActorPicker.options.FindIndex(x => x.text == unit.Actor.Name);

			UnitNameText.text = unit.Name;
			UnitSpeedText.text = unit.Speed + "";

			UnitButton.onClick.AddListener(OnClick);

			Unit = unit;
		}

		public void OnClick()
		{
			ClickCallback.Invoke(Unit);
		}
	}
}
