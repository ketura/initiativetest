﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	[RequireComponent(typeof(Dropdown))]
	public class DropdownClearDefault : MonoBehaviour 
	{
		public string DefaultName;
		private Dropdown dd;
		public void Awake()
		{
			dd = gameObject.GetComponent<Dropdown>();
			dd.onValueChanged.AddListener(ClearDropdown);
			dd.options.Add(new Dropdown.OptionData(DefaultName));
			dd.itemText.text = DefaultName;
			dd.value = 0;
		}
		public void ClearDropdown(int index)
		{
			if (dd.options.Count > 0)
			{
				dd.itemText.text = dd.options.ToArray()[index].text;
			}
		}

		public void AddRange(IEnumerable<string> items)
		{
			dd.ClearOptions();
			foreach (string s in items)
			{
				dd.options.Add(new Dropdown.OptionData(s));
			}

			dd.options.Add(new Dropdown.OptionData(DefaultName));
			dd.value = dd.options.Count - 1;
			dd.options.RemoveAt(dd.options.Count - 1);

		}
	}

	
}
