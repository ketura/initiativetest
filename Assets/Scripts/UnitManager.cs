﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class UnitManager : MonoBehaviour 
	{
		[SerializeField]
		public Dictionary<string, Unit> Units;

		private SpeciesManager SpeciesManager;
		private ActorManager ActorManager;

		public IEnumerable<string> UnitNames
		{
			get
			{
				return Units.Keys;
			}
		}

		public Unit GetUnit(string name)
		{
			if (!Units.ContainsKey(name))
				return null;

			return Units[name];
		}

		public void AddUnit(Unit unit)
		{
			if (Units.ContainsKey(unit.Name))
			{
				Debug.Log(string.Format("Warning!  Unit {0} has been submitted again to the Action Registery.", unit.Name));
			}

			Units[unit.Name] = unit;
			unit.transform.parent = this.transform;
		}

		public Unit CreateUnit(string name, string species, string actor, int speed)
		{
			GameObject go = new GameObject(name);
			var unit = go.AddComponent<Unit>();
			unit.Name = name;
			unit.SpeciesType = SpeciesManager.GetSpecies(species);
			unit.Actor = ActorManager.GetActor(actor);
			unit.Speed = speed;

			return unit;
		}

		public void DestroyUnit(Unit unit)
		{
			Units.Remove(unit.Name);
			Destroy(unit.gameObject);
		}

		public void DestroyUnit(string name)
		{
			DestroyUnit(Units[name]);
		}

		void Awake () 
		{
			SpeciesManager = GameObject.FindGameObjectWithTag("SpeciesManager").GetComponent<SpeciesManager>();
			ActorManager = GameObject.FindGameObjectWithTag("ActorManager").GetComponent<ActorManager>();
			Units = new Dictionary<string, Unit>();
		}

		void Update () 
		{
		
		}
	}
}
