﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Utilities

namespace test
{
	public class UserActor : RoundActor
	{
		public Button ReadyButton;
		public Dropdown ActionList;
		public Button AddActionButton;

		public Unit CachedUnit;

		protected override void Start()
		{
			base.Start();

			
		}

		public override void Initialize(RoundManager rm, Unit unit)
		{
			base.Initialize(rm, unit);

			CachedUnit = unit;

			ReadyButton = RoundManager.ReadyButton;
			ActionList = RoundManager.ActionPicker;
			AddActionButton = RoundManager.ActionSelect;

			if (ActionList != null)
			{
				ActionList.options.Clear();
				ActionList.options.Add(new Dropdown.OptionData(ActionList.GetComponent<DropdownClearDefault>().DefaultName));
				foreach (Action a in unit.Moves)
				{
					ActionList.options.Add(new Dropdown.OptionData(a.Name));
				}
				ActionList.value = 0;
			}
			else
			{
				Debug.Log("User controls not set up!");
			}

			if(ReadyButton != null)
				ReadyButton.onClick.AddListener(ReadyClicked);

			if (AddActionButton != null)
				AddActionButton.onClick.AddListener(AddMoveClicked);
		}

		public override void StartPlanning(Unit unit)
		{
			base.StartPlanning(unit);

			unit.MoveMessage = "";
		}



		public void ReadyClicked()
		{
			CachedUnit.Ready = true;
		}

		public void AddMoveClicked()
		{
			CachedUnit.PlannedActions.Add(ActionManager.GetAction(ActionList.options[ActionList.value].text));
			CachedUnit.MoveMessage = MoveMessage(CachedUnit);
		}

		public override void ActorUpdate(Unit unit)
		{
			base.ActorUpdate(unit);
			unit.Finished = true;
		}

	}
}
