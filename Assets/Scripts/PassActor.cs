﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using Utilities

namespace test
{
	public class PassActor: RoundActor
	{
		protected override void Start()
		{
			base.Start();
		}

		public override void ActorUpdate(Unit unit)
		{
			base.ActorUpdate(unit);
			
			unit.Finished = true;
		}

		public override void StartPlanning(Unit unit)
		{
			base.StartPlanning(unit);
			unit.PlannedActions.Add(ActionManager.GetAction("Pass"));
			unit.Ready = true;
		}
	}
}