﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace test
{
	[ExecuteInEditMode]
	public class FrigginPanelReposition : MonoBehaviour 
	{
		RectTransform RT;
		void Start () 
		{
			RT = this.RT();
		}

		void Update () 
		{
			//RT.DebugOutput();
			if(RT.anchoredPosition.x != 0)
			{
				RT.anchoredPosition = new Vector2(0, RT.anchoredPosition.	y);
			}
		}
	}
}
